package student;

import student.Student;

import java.sql.Date;
import java.util.Scanner;

public class StudentPracticalExercise {

	public StudentPracticalExercise() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws IllegalBirthDayException, IllegalGPAException{
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		Student student = new Student();
		int id;
		String name;
		Date birthday = Date.valueOf("0000-00-00");
		float gpa;
		
		System.out.print("Enter the student's ID: ");
		id = Integer.parseInt(keyboard.nextLine());
		System.out.print("Enter the name of student: ");
		name = keyboard.nextLine();
		System.out.print("Enter the birthday (yyyy-mm-dd): ");
		birthday = Date.valueOf(keyboard.nextLine());
		
		System.out.print("Enter the gpa: ");
		gpa = Float.parseFloat(keyboard.nextLine());
		
		student = new Student(id, name, birthday, gpa);
		
		
		keyboard.close();
	}

}
