package student;

import java.sql.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Student {
	private int studentID;
	private String studentName;
	private Date birthday;
	private float gpa;

	public int getStudentID() {
		return studentID;
	}

	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public float getGpa() {
		return gpa;
	}

	public void setGpa(float gpa) {
		this.gpa = gpa;
	}

	public Student(int id, String name, Date birthday, float gpa) throws IllegalBirthDayException, IllegalGPAException{
		this.setStudentID(id);
		this.setStudentName(name);
		
		if (gpa < 0 || gpa > 4) {
			System.out.println("ERR: INVALID GPA ");
			throw(new IllegalGPAException());
		}
		this.setBirthday(birthday);
		
		this.setGpa(gpa);
	}
		
	public Student() {
		
	}

}
