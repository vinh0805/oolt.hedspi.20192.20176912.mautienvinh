package media;

import aims.PlayerException;

public interface Playable {
	public void play() throws PlayerException;

}
