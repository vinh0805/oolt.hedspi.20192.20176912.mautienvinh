package media;

public abstract class Media {
	private String title;
	private String category;
	private float cost;
	
	//Constructors
	public Media(String title) {
		this.title = title;
	}
	
	public Media(String title, String category) {
		this(title);
		this.category = category;
	}
	
	public Media(String title, String category, float cost) {
		this(title, category);
		this.cost = cost;
	}

	public Media() {
		// TODO Auto-generated constructor stub
	}

	
	// getter and setter
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}


	@Override
	public boolean equals(Object obj) {
		try {
			Media o = (Media)obj;
			if(o.getTitle() == this.getTitle() && o.getCost() == this.getCost()) {
				return true;
			} else return false;
		} catch (NullPointerException e) {
			e.getMessage();
			return false;
		} catch (ClassCastException e) {
			e.printStackTrace();
			return false;
		}
	}

}
