package media;

import java.util.ArrayList;
import java.util.List;

import aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {

	private String artist;
	private int totalLength;
	private List<Track> tracks = new ArrayList<Track>();
	
	public void play() throws PlayerException {
		if(this.getLength() <= 0) {
			System.err.println("ERROR: DVD length is 0");
			throw(new PlayerException());
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
		
		java.util.Iterator iter = tracks.iterator();
		Track nextTrack;
		
		while(iter.hasNext()) {
			nextTrack = (Track) iter.next();
			try {
				nextTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
	}	
	public void addTrack(Track track) {
		for(Track i: tracks) {
			if(i.equals(track) == true) {
				System.out.println("This track is existing in list!");
				return;
			} 
		}
		tracks.add(track);
	}
	
	public void removeTrack(Track track) {
		for(Track i: tracks) {
			if(i.equals(track) == true) {
				System.out.println("This track was deleted!");
				tracks.remove(i);
				return;
			} 
		}
		System.out.println("This track doesn't exist in the list!");
	}

	// Constructor
	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	
	public CompactDisc(String title, String category, float cost, String director) {
		super(title, category, cost, director);
//		this.setDirector(director);		
	}
		

	public int getTotalLength() {
		for (Track i : tracks) {
			totalLength += i.getLength();
		}
		return totalLength;
	}

	@Override
	public int compareTo(CompactDisc o) {
		// sort by title
		//return super.getTitle().compareTo(o.getTitle());
		
		// sort by number of tracks
		if(tracks.size() == o.getTracks().size()) {			// sort by totalLength if number of tracks is equal
			return Integer.compare(getTotalLength(), o.getTotalLength());
		}													// sort by number of tracks
		return Integer.compare(tracks.size(), o.getTracks().size());
	}

	
	// getter and setter
	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	

}
