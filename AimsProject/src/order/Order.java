package order;

import java.util.ArrayList;
import java.util.Date;

import media.Media;

public class Order {
	public static final int MAX_NUMBER_ORDERED = 6;
	public static final int MAX_LIMITED_ORDERD = 5;
	
	private float totalCost = 0;
	private Date dateOrdered;
	private static int nbOrders = 0;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	

	//constructor
	public Order() {
		
	}
	
	public void addMedia(Media dvd) {
		itemsOrdered.add(dvd);
		//System.out.println("The list is full!!!");
	}
	
	public void removeMedia(Media dvd) {
		for (Media i : itemsOrdered) {
			if (i == dvd) {
				itemsOrdered.remove(i);
				System.out.println(dvd.getTitle() + " has been deleted!!");
				return;
			}
		}
		System.out.println("Does not exist this dvd in ordered list!!!");
	}
	
	
	public void removeMedia(String title) {
		for (Media i : itemsOrdered) {
			if (i.getTitle() == title) {
				System.out.println(i.getTitle() + " has been deleted!!");
				itemsOrdered.remove(i);
				return;
			}
		}
		System.out.println("Does not exist this dvd in ordered list!!!");
	}


	public void printList() {
		int i2 = 1;
		System.out.println("All the items in the list: ");
		for (Media i: itemsOrdered) {
			System.out.println("\t" + i2 + ": " + i.getTitle() + " - " + i.getCategory() + " - " + i.getCost());
			i2++;
		}
	}
	
	public void printTotalCost() {
		totalCost = 0;
		for(Media i: itemsOrdered) {
			totalCost += i.getCost();
		}
		System.out.println("The total cost is: " + totalCost);
	}


	
	// getter and setter

	public static int getNbOrders() {
		return nbOrders;
	}
	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
	public static void updateNbOrders() {
		setNbOrders(getNbOrders() + 1);
	}
	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}


	public float getTotalCost() {
		return totalCost;
	}
	public ArrayList<Media> getItemsOrdered() {
		return itemsOrdered;
	}
	public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
		this.itemsOrdered = itemsOrdered;
	}
	

}
/*	
	public void printOrderedList() {
		System.out.println("\n********************************Order********************************");
		System.out.println("Date: " + getDateOrdered());
		for(int i = 0; i < qtyOrdered; i++) {
			System.out.println(i + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() + " - " + 
								itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + " - " + itemsOrdered[i].getCost() + "$");
		}
		System.out.println("Total cost: " + getTotalCost());
	}
	
	public DigitalVideoDisc getALuckyItem () {
		int luckyNumber = (int)(Math.random()*qtyOrdered);
		System.out.println("The lucky disc: " + (luckyNumber + 1) + ". " + itemsOrdered[luckyNumber].getTitle());
		totalCost -= itemsOrdered[luckyNumber].getCost();
		itemsOrdered[luckyNumber].setCost(0);
		return itemsOrdered[luckyNumber];
	}
}
*/