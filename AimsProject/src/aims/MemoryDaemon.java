package aims;

public class MemoryDaemon implements java.lang.Runnable{
	
	//private Thread t1;
	private long memoryUsed = 0;
	
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		/*
		t1 = new Thread();
		t1.setDaemon(true);
		t1.start();
		*/
		while(true) {
			used = rt.totalMemory() - rt.freeMemory();
			if(used != memoryUsed) {
				System.out.println("\tMemory used = " + used);
				memoryUsed = used;
			}
		}
	}
	
	
	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}

}
