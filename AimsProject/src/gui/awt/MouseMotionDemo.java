package gui.awt;

import java.awt.*;
import java.awt.event.*;

public class MouseMotionDemo extends Frame implements MouseListener, MouseMotionListener, WindowListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TextField tfMouseClickX;
	private TextField tfMouseClickY;
	
	private TextField tfMousePositionX;
	private TextField tfMousePositionY;

	public MouseMotionDemo() {
		setLayout(new FlowLayout());
		
		add(new Label("X-Click: "));
		tfMouseClickX = new TextField(10);
		tfMouseClickX.setEditable(false);
		add(tfMouseClickX);
		
		add(new Label("Y-Click: "));
		tfMouseClickY = new TextField(10);
		tfMouseClickY.setEditable(false);
		add(tfMouseClickY);
		
		add(new Label("X-Position: "));
		tfMousePositionX = new TextField(10);
		tfMousePositionX.setEditable(false);
		add(tfMousePositionX);
		
		add(new Label("Y-Position: "));
		tfMousePositionY = new TextField(10);
		tfMousePositionY.setEditable(false);
		add(tfMousePositionY);
		
		addMouseListener(this);
		addWindowListener(this);
		addMouseMotionListener(this);
		
		setTitle("MouseMotion Demo");
		setVisible(true);
		setSize(400, 120);
	}
	
	public static void main(String[] args) {
		new MouseMotionDemo();
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		tfMousePositionX.setText(e.getX() + "");
		tfMousePositionY.setText(e.getY() + "");
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		tfMouseClickX.setText(e.getX() + "");
		tfMouseClickY.setText(e.getY() + "");
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
