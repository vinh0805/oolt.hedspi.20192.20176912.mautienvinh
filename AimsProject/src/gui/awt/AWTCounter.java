package gui.awt;

import java.awt.*;			// Using AWT container and component classes
import java.awt.event.*;	// Using AWT event classes and listener intefaces

// An AWT program inherits from the top-level container java.awt.Frame
public class AWTCounter extends Frame implements ActionListener, WindowListener{
	private static final long serialVersionUID = 1L;
	private Label lblCount;		// Declare a label component
	private TextField tfCount;	// Declare a textField component
	private Button btnCount;	// Declare a button component
	private int count = 0;
	
	// Construtor to setup GUI components and event handlers
	public AWTCounter() {
		setLayout(new FlowLayout());
			// "super" Frame, which is Container, sets its layout to FlowLayout to arrange 
			// the components from left to right, and flow to next row from top-to-bottom.
		
		lblCount = new Label("Counter");			// Constructor the label component
		add(lblCount);								// "super" Frame container adds Label component
		
		tfCount = new TextField(count + "", 10);	// construct the TextField component with initial text
		tfCount.setEditable(false);					// set to read-only
		add(tfCount);								// "super" Frame container adds TextField component
		
		btnCount = new Button("Count");
		add(btnCount);
		
		btnCount.addActionListener(this);
			// "btnCount" is the source object that fires an ActionEvent when clicked
			// The source add "this" instance as an ActionEvent listener, which provides
			// an ActionEvent handler called actionPerformed().
			// Clicking "btnCount" invokes actionPerformed();
		
		setTitle("AWT Counter");					// "super" Trame sets its title
		setSize(250, 100);							// "super" Frame sets its initial window size
		
		// For inspecting the Container/ Components objects
		 System.out.println(this);
		 System.out.println(lblCount);
		 System.out.println(tfCount);
		 System.out.println(btnCount);
		
		setVisible(true);							// "super" Frame shows
		 System.out.println(this);
		 System.out.println(lblCount);
		 System.out.println(tfCount);
		 System.out.println(btnCount);		
		
		addWindowListener(this);
	}
	
	// The entry main() method
	public static void main(String[] args) {
		// Invoke the constructor to setup the GUI, by allocating an instance
		new AWTCounter();
			// only simply "new AWTCounter();" for an anonymous instance
	}
	
	// ActionEvent handler - Called backed upon button-click.
	@Override
	public void actionPerformed(ActionEvent evt) {
		++count;			// Increase the counter value
							// Display the counter value on the TextField tfCount
		tfCount.setText(count + "");	// Convert int to String	
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Hello nigga.(Lo cc)");

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Hello nigga. (Iconified)");
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Hello nigga. (Deiconified)");
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Hello nigga. (Activated)");
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("Hello nigga. (Deactivated)");
	}
}
