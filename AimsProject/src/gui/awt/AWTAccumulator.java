package gui.awt;

import java.awt.*;
import java.awt.event.*;

// An AWT GUI program inherits from the top-level container java.awt.Frame 
public class AWTAccumulator extends Frame implements ActionListener, WindowListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Label lblInput;		// Declare input Label
	private Label lblOutput;	// Declare output Label
	private TextField tfInput;	// Declare input TextField
	private TextField tfOutput;	// Declare output TextField
	private int sum = 0;
	
	// Constructor to setup the GUI components and event handlers	
	public AWTAccumulator() {
		setLayout(new FlowLayout());
		
		lblInput = new Label("Enter an integer: ");
		add(lblInput);
		
		tfInput = new TextField(10);
		add(tfInput);
		tfInput.addActionListener(this);
		
		lblOutput = new Label("The accumulated sum is: ");
		add(lblOutput);
		
		tfOutput = new TextField(10);
		tfOutput.setEditable(false);
		add(tfOutput);
		
		setTitle("AWT Accumulator");
		setSize(350, 120);
		setVisible(true);
		
		addWindowListener(this);
	}

	public static void main(String[] args) {
		new AWTAccumulator();
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		int numberIn = Integer.parseInt(tfInput.getText());
		sum += numberIn;
		tfInput.setText("");
		tfOutput.setText(sum + "");
	}
	
	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


}
