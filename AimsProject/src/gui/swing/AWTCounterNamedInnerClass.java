// Example 7
package gui.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AWTCounterNamedInnerClass extends JFrame {
	public TextField tfCount;
	public Button btnCount;
	private int count = 0;

	public AWTCounterNamedInnerClass() {
		setLayout(new FlowLayout());
		add(new Label("Counter"));
		tfCount = new TextField("0", 10);
		add(tfCount);
		
		btnCount = new Button("Count");
		add(btnCount);

		btnCount.addActionListener(new BtnCountListener());;
		
		setTitle("AWT Counter");
		setSize(250, 100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		new AWTCounterNamedInnerClass();
	}
	
	private class BtnCountListener implements ActionListener {
		@Override
		public void actionPerformed (ActionEvent e) {
			++count;
			tfCount.setText(count + "");
		}
	}

}
