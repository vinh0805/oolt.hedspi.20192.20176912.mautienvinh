// Example 6
package gui.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import javax.swing.JFrame;

public class KeyEventDemo extends JFrame implements KeyListener {

	private TextField tfInput;
	private TextArea taDisplay;
	public KeyEventDemo() {
		setLayout(new FlowLayout());
		
		add(new Label("Enter text: "));
		tfInput = new TextField(10);
		add(tfInput);
		taDisplay = new TextArea(5, 40);
		add(taDisplay);
		
		tfInput.addKeyListener(this);
		
		setTitle("KeyEvent Demo");
		setSize(400, 200);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new KeyEventDemo();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		taDisplay.append("You have typed " + e.getKeyChar() + "\n");
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	

}
