// Example 8
package gui.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AWTCounterAnonymosInnerClass extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;

	public AWTCounterAnonymosInnerClass() {
		setLayout(new FlowLayout());
		add(new Label("Counter"));
		tfCount = new TextField("0", 10);
		tfCount.setEditable(false);
		add(tfCount);
		
		btnCount = new Button("Count");
		add(btnCount);
		
		btnCount.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				++count;
				tfCount.setText(count + "");
			}
		});
		
		setTitle("AWT Counter");
		setSize(250, 100);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new AWTCounterAnonymosInnerClass();

	}

}
