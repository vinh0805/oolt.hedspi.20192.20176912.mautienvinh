package gui.swing;

import java.awt.*;
import javax.swing.*;

public class SwingDemo extends JFrame {

	public SwingDemo() {
		// Get all the content-pane of this JFrame, which is a java.awt.Container
		// All operations, such as setLayout() and add() operate on the content-pane
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		cp.add(new JLabel("Hello world!"));
		cp.add(new JButton("Button"));
	}
}
