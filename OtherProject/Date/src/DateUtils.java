package src;

public class DateUtils {
	public static int compareTwoDates(MyDate o1, MyDate o2) {
		if (o1.getYear() > o2.getYear())
			return 1;
		else if (o1.getYear() < o2.getYear())
			return -1;
		else {
			if (o1.getMonth() > o2.getMonth())
				return 1;
			else if (o1.getMonth() < o2.getMonth())
				return -1;
			else {
				if (o1.getDay() > o2.getDay())
					return 1;
				else if (o1.getDay() < o2.getDay())
					return -1;
				else return 0;
			}
		}
	}
	
	public static void sortDates(MyDate o1, MyDate o2) {
		if(DateUtils.compareTwoDates(o1,o2) == -1) {
			int day,month,year;
			String date;
			
			date = o1.getDate();
			day = o1.getDay();
			month = o1.getMonth();
			year = o1.getYear();
			
			o1.setYear(o2.getYear());
			o1.setMonth(o2.getMonth());
			o1.setDay(o2.getDay());
			o1.setDate(o2.getDate());
			
			o2.setDate(date);
			o2.setYear(year);
			o2.setMonth(month);
			o2.setDay(day);
		}
	}
}
