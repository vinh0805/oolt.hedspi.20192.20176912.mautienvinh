//Example 7: Add Two Matrices of the same size

import java.util.Scanner;
public class Ex7_AddTwoMatrices {
	public static void main(String args[]) {
		final int MAX_SIZE = 10;
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Enter the size of two matrices: ");
		int sizeOfMatrices = keyboard.nextInt();
		
		int arrayA[][] = new int[MAX_SIZE][MAX_SIZE];
		int arrayB[][] = new int[MAX_SIZE][MAX_SIZE];
		
		//Enter the array A
		for(int i = 0; i < sizeOfMatrices; i++) {
			for(int j = 0; j < sizeOfMatrices; j++) {
				System.out.print("Enter the value of A" + i + j + ": ");
				arrayA[i][j] = keyboard.nextInt();
			}
		}
		
		//Show the array A
		System.out.println("The matrice A:");
		for(int i = 0; i < sizeOfMatrices; i++) {
			for(int j = 0; j < sizeOfMatrices; j++) {
				System.out.print(arrayA[i][j] + " ");
				if(j == sizeOfMatrices - 1) System.out.print("\n");
			}
		}

		//Enter the array B
		for(int i = 0; i < sizeOfMatrices; i++) {
			for(int j = 0; j < sizeOfMatrices; j++) {
				System.out.print("Enter the value of B" + i + j + ": ");
				arrayB[i][j] = keyboard.nextInt();
			}
		}

		//Show the array B
		System.out.println("The matrice B:");
		for(int i = 0; i < sizeOfMatrices; i++) {
			for(int j = 0; j < sizeOfMatrices; j++) {
				System.out.print(arrayB[i][j] + " ");
				if(j == sizeOfMatrices - 1) System.out.print("\n");
			}
		}
	}
}