//Example 6: Sort an array

import java.util.Scanner;

public class Ex6_SortArray {
	public static void main(String args[]) {
		final int MAX_LENGTH = 20;
		Scanner keyboard = new Scanner(System.in);
		int sum = 0;
		double average = 0;
		
		//Nhap mang
		System.out.print("Enter the number of elements in array: ");
		int iNumOfArray = keyboard.nextInt();
		int arrayA[] = new int[MAX_LENGTH];
		arrayA[iNumOfArray] = '\0';
		for(int i = 0; i < iNumOfArray; i++) {
			System.out.print("Enter the " + (i + 1));
			if(i == 0) System.out.print("st ");
			else if(i == 1) System.out.print("nd ");
			else System.out.print("th ");
			System.out.print("number: ");
			arrayA[i] = keyboard.nextInt();
			sum += arrayA[i];
		}
		average = sum/iNumOfArray;
		
		//Sap xep mang
		int temp = arrayA[0];
		for(int i = 0; i < iNumOfArray - 1; i++) {
			for(int j = i + 1; j < iNumOfArray; j++) {
				if(arrayA[i] > arrayA[j]) {
					temp = arrayA[j];
					arrayA[j] = arrayA[i];
					arrayA[i] = temp;
				}
			}
		}
		
		//In mang
		System.out.print("The sorted array: ");
		for(int i = 0; i < iNumOfArray; i++) {
			System.out.print(arrayA[i] + " ");
		}
		System.out.println("\nThe sum of array: " + sum);
		System.out.println("The average of array: " + average);

	}	
}
