//Example 5: Caculate two numbers
import javax.swing.JOptionPane;
public class Ex5_CaculateTwoNumbers {
	public static void main(String args[]) {
		String strNum1, strNum2;
		
		strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input first number", JOptionPane.INFORMATION_MESSAGE);
		strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
		
		Double Num1 = Double.parseDouble(strNum1);
		Double Num2 = Double.parseDouble(strNum2);
		
		double Sum = Num1 + Num2;
		double Difference = Num1 - Num2;
		double Product = Num1 * Num2;
		double Quotient = Num1 / Num2;
		
		JOptionPane.showMessageDialog(null, "Sum of two number: " + Sum, "Sum", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Difference of two number: " + Difference, "Difference", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Product of two number: " + Product, "Product", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Quotient of two number: " + Quotient, "Quotient", JOptionPane.INFORMATION_MESSAGE);
	}
}
